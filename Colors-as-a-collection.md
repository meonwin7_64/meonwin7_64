using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Windows.UI;

//UWP example
//https://github.com/Microsoft/Windows-task-snippets/blob/1a40a94cab7db26494920f67fa3c1273698f7c8e/tasks/Colors-as-a-collection.md
//Note that I'm using Atlassian Bitbucket for this

public static IEnumerable<Color> GetAllColors()
{
    return typeof(Colors).GetRuntimeProperties().Select(x => (Color)x.GetValue(null));
}